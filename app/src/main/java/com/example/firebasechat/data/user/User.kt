package com.example.firebasechat.data.user

import android.os.Parcelable
import com.example.firebasechat.common.Constans
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var uid: String? = "",
    var email: String? = "",
    var name: String? = "",
    var role: String? = "",
    var user_number: String? = "",
    var photo_url: String? = ""
) : Parcelable {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            Constans.UID_FIELD to uid,
            Constans.EMAIL_FIELD to email,
            Constans.NAME_FIELD to name,
            Constans.ROLE_FIELD to role,
            Constans.USER_NUMBER_FIELD to user_number,
            Constans.PHOTO_URL to photo_url
        )
    }
}