package com.example.firebasechat.view.activity

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.recyclerview.widget.GridLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.common.Utilities
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.data.group.GroupChat
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.adapter.GroupMediaAdapter
import com.google.firebase.database.DataSnapshot
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_group_media.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class GroupMediaActivity : BaseActivity() {
    private val mediaList = mutableListOf<GroupChat>()
    lateinit var mAdapter: GroupMediaAdapter
    private var myDownloadDid: Long = 0
    var group: Group? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_media)
        val mediaType = intent?.getStringExtra(Constans.MESSAGE_TYPE_FIELD)
        group = intent?.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        initView(mediaType)
        getData(mediaType)
        val br = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
                if (id == myDownloadDid) {
                    context?.toast("Download Completed")
                }
            }
        }

        registerReceiver(br, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }

    private fun getData(mediaType: String?) {
        val query = group?.gid?.let {
            mDatabase.child(Constans.CHAT_GROUP_TABLE_FIREBASE).child(it)
                .orderByChild(Constans.MESSAGE_TYPE_FIELD).equalTo(mediaType)
        }
        query?.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                mediaList.clear()
                Logger.addLogAdapter(AndroidLogAdapter())
                Logger.d(snapshot)
                Logger.d(mediaType)
                for (i in snapshot.children) {
                    val data = i.getValue(GroupChat::class.java)
                    data?.let { mediaList.add(it) }
                }
                mAdapter.setData(mediaList)
            }
        })
    }

    private fun initView(mediaType: String?) {
        rvMediaGroup?.apply {
            layoutManager = GridLayoutManager(this@GroupMediaActivity, 2)
            mAdapter = if (mediaType == Constans.MESSAGE_TYPE_FILE) {
                GroupMediaAdapter {
                    checkWritePermission(it)
                }
            } else {
                GroupMediaAdapter {
                    startActivity<PhotoViewerActivity>(Constans.IMG_PATH to it.message)
                    it.message?.let { it1 -> toast(it1) }
                }
            }
            adapter = mAdapter
        }

    }

    private fun checkWritePermission(it: GroupChat) {
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionChecker.PERMISSION_GRANTED -> {
                downloadFile(it)
            }
            PermissionChecker.PERMISSION_DENIED -> {
                //Request Location Permission
                Utilities.checkWritePermission(this)
            }
        }
    }

    private fun downloadFile(it: GroupChat) {

//                val intent = Intent(Intent.ACTION_VIEW, chat?.message?.toUri())
//                context.startActivity(intent)
        toast("Downloading File...")
        val fileUri = DownloadManager.Request(Uri.parse(it.message))
            .setTitle("Download File")
            .setDescription("Downloading File...")
        fileUri.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            it.file_name
        )
        fileUri.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
        myDownloadDid = dm?.enqueue(fileUri)!!
    }
}