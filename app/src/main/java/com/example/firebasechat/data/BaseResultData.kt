package com.example.firebasechat.data

import com.google.gson.annotations.Expose

/**
 * Created by ooyama on 2017/05/26.
 */

open class BaseResultData {
    @Expose
    var meta: Any? = null

    @Expose
    var link: Any? = null

}

open class APIError {
    val error: ErrorData = ErrorData()
}

