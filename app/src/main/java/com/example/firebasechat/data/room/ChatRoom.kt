package com.example.firebasechat.data.room


data class ChatRoom(
    var uid1: String,
    var uid2: String
)