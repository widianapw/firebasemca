package com.example.firebasechat.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity() {
    private lateinit var mAuth: FirebaseAuth
    private val firebaseDatabase = FirebaseDatabase.getInstance()
    private var googleSignInClient: GoogleSignInClient? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
        mAuth = FirebaseAuth.getInstance()

        btnSignInGoogleLogin?.setOnClickListener {
            val signInIntent = googleSignInClient?.signInIntent
            startActivityForResult(signInIntent, Constans.RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constans.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                fireBaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                toast(e.localizedMessage)
            }
        }
    }

    private fun fireBaseAuthWithGoogle(account: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) {
                if (it.isSuccessful) {
                    if (it.result?.additionalUserInfo?.isNewUser!!) {
                        val user = mAuth.currentUser
                        val userData = HashMap<String, Any?>()
                        userData[Constans.EMAIL_FIREBASE] = user?.email
                        userData[Constans.UID_FIREBASE] = user?.uid
                        userData[Constans.PHOTO_URL] = user?.photoUrl.toString()
                        userData[Constans.NAME_FIELD] = user?.displayName
                        userData[Constans.ROLE_FIELD] = ""
                        userData[Constans.USER_NUMBER_FIELD] = ""
                        val reference = firebaseDatabase.getReference(Constans.USERS_TABLE_FIREBASE)
                        user?.uid?.let { data -> reference.child(data).setValue(userData) }
                    }
                    startActivity<MainTabActivity>()
                    finishAffinity()
                } else {
                    toast("Failed to login")
                }
            }
    }

    override fun onResume() {
        super.onResume()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            startActivity<MainTabActivity>()
            finish()
        }
    }
}