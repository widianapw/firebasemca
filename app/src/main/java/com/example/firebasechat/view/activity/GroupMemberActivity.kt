package com.example.firebasechat.view.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.data.group.GroupUser
import com.example.firebasechat.data.user.User
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.adapter.GroupUserAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.DataSnapshot
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_group_member.*
import kotlinx.android.synthetic.main.dialog_detail_user.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class GroupMemberActivity : BaseActivity() {
    val memberList = mutableListOf<GroupUser>()
    val userMember = mutableListOf<GroupUser>()
    var mAdapter: GroupUserAdapter? = null
    var mGroupUser: GroupUser? = null
    var group: Group? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        Logger.addLogAdapter(AndroidLogAdapter())
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_member)
        group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        val query = group?.gid?.let {
            mUser?.uid?.let { it1 ->
                mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it).child(it1)
            }
        }

        val allMember = group?.gid?.let {
            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it)
                .orderByChild(Constans.IS_ADMIN_FIELD).equalTo(Constans.USER)
        }
        allMember?.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                userMember.clear()
                Logger.d(snapshot)
                for (i in snapshot.children) {
                    val data = GroupUser(
                        i.child(Constans.UID_FIELD).value.toString(),
                        i.child(Constans.EMAIL_FIELD).value.toString(),
                        i.child(Constans.ALLOW_TYPING_FIELD).value.toString(),
                        i.child(Constans.STATUS_FIELD).value.toString(),
                        i.child(Constans.IS_ADMIN_FIELD).value.toString()
                    )
                    userMember.add(data)
                }
            }
        })

        query?.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                val groupUser = GroupUser(
                    snapshot.child(Constans.UID_FIELD).value.toString(),
                    snapshot.child(Constans.EMAIL_FIELD).value.toString(),
                    snapshot.child(Constans.ALLOW_TYPING_FIELD).value.toString(),
                    snapshot.child(Constans.STATUS_FIELD).value.toString(),
                    snapshot.child(Constans.IS_ADMIN_FIELD).value.toString()
                )
                val test = snapshot.getValue(GroupUser::class.java)
                Logger.d(test)
                mGroupUser = groupUser
                setAuthority(groupUser)
            }
        })

    }

    private fun setAuthority(groupUser: GroupUser) {
        val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        mAdapter = when (groupUser.is_admin) {
            Constans.ADMIN -> {
                GroupUserAdapter(Constans.ADMIN, group!!) {
                    showUserDetailDialog(it)
                }
            }
            Constans.SUPER_ADMIN -> {
                GroupUserAdapter(Constans.SUPER_ADMIN, group!!) {
                    showUserDetailDialog(it)
                }
            }
            else -> {
                GroupUserAdapter(Constans.USER, group!!) {
                    showUserDetailDialog(it)
                }
            }
        }

        rvUserGroup.apply {
            layoutManager = LinearLayoutManager(this@GroupMemberActivity)
            adapter = mAdapter
        }

        if (group.admin == mUser?.email || groupUser.is_admin == Constans.ADMIN || groupUser.is_admin == Constans.SUPER_ADMIN) {
            btnMuteAll.visibility = View.VISIBLE
            btnUnmuteAll.visibility = View.VISIBLE
            val query = group.gid?.let {
                mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(
                    it
                )
            }
            query?.addValueEventListener(object : ValueEventListenerAbstract() {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Logger.addLogAdapter(AndroidLogAdapter())
                    memberList.clear()
                    for (i in snapshot.children) {
                        val data = GroupUser(
                            i.child(Constans.UID_FIELD).value.toString(),
                            i.child(Constans.EMAIL_FIELD).value.toString(),
                            i.child(Constans.ALLOW_TYPING_FIELD).value.toString(),
                            i.child(Constans.STATUS_FIELD).value.toString(),
                            i.child(Constans.IS_ADMIN_FIELD).value.toString()
                        )
                        data.let { memberList.add(it) }
                    }
                    mAdapter?.setData(memberList)
                }
            })

            btnMuteAll?.setOnClickListener {
                toast("Mute All")
                muteAll(group)
            }

            btnUnmuteAll?.setOnClickListener {
                toast("Unmute All")
                unmuteAll(group)
            }


        } else {
            btnMuteAll?.visibility = View.GONE
            btnUnmuteAll?.visibility = View.GONE
            val query = group.gid?.let {
                mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(
                    it
                )
            }
            query?.addValueEventListener(object : ValueEventListenerAbstract() {
                override fun onDataChange(snapshot: DataSnapshot) {
                    memberList.clear()
                    Logger.addLogAdapter(AndroidLogAdapter())
                    Logger.d(group)
                    for (i in snapshot.children) {
                        if (i.child(Constans.STATUS_FIELD).value.toString() == Constans.STATUS_APPROVED) {
                            val data = GroupUser(
                                i.child(Constans.UID_FIELD).value.toString(),
                                i.child(Constans.EMAIL_FIELD).value.toString(),
                                i.child(Constans.ALLOW_TYPING_FIELD).value.toString(),
                                i.child(Constans.STATUS_FIELD).value.toString(),
                                i.child(Constans.IS_ADMIN_FIELD).value.toString()
                            )
                            Logger.d(data)
                            data.let { memberList.add(it) }
                        }
                    }
                    mAdapter!!.setData(memberList)
                }
            })
        }

    }

    private fun unmuteAll(group: Group?) {
        for (i in userMember) {
            i.allow_typing = Constans.ENABLE_TYPING
            group?.gid?.let {
                i.uid?.let { it1 ->
                    mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it).child(
                        it1
                    ).updateChildren(i.toMap())
                }
            }
        }
    }

    private fun muteAll(group: Group?) {
        for (i in userMember) {
            i.allow_typing = Constans.DISABLE_TYPING
            group?.gid?.let {
                i.uid?.let { it1 ->
                    mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it).child(
                        it1
                    ).updateChildren(i.toMap())
                }
            }

        }
    }

    private fun showUserDetailDialog(member: GroupUser) {
        val dialog = layoutInflater.inflate(R.layout.dialog_detail_user, null)
        val btnSetAdmin = dialog.btnSetAdmin
        val query1 = group?.gid.let { it1 ->
            member.uid?.let { it2 ->
                it1?.let { it3 ->
                    mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it3).child(it2)
                }
            }
        }
        val ref = member.uid?.let { mDatabase.child(Constans.USERS_TABLE_FIREBASE).child(it) }
        ref?.addListenerForSingleValueEvent(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data = snapshot.getValue(User::class.java)
                dialog.apply {
                    Glide.with(this).load(data?.photo_url).placeholder(R.color.colorAlto)
                        .into(ivUserDetail)
                    findViewById<TextView>(R.id.tvEmail)?.text = data?.email
                    findViewById<TextView>(R.id.tvName)?.text = data?.name
                    findViewById<TextView>(R.id.tvRole)?.text = data?.role
                    findViewById<TextView>(R.id.tvNim)?.text = data?.user_number
                    if (member.uid != mUser?.uid) {
                        if ((mGroupUser?.is_admin == Constans.SUPER_ADMIN) && (member.is_admin == Constans.USER)) {
                            btnSetAdmin?.visibility = View.VISIBLE
                            btnRemoveAdmin?.visibility = View.GONE
                        } else if ((mGroupUser?.is_admin == Constans.SUPER_ADMIN) && (member.is_admin == Constans.ADMIN)) {
                            btnSetAdmin?.visibility = View.GONE
                            btnRemoveAdmin?.visibility = View.VISIBLE
                        } else {
                            btnSetAdmin?.visibility = View.GONE
                            btnRemoveAdmin?.visibility = View.GONE
                        }
                    } else {
                        btnSetAdmin?.visibility = View.GONE
                        btnRemoveAdmin?.visibility = View.GONE
                    }

                }
            }
        })

        val dialogView = MaterialAlertDialogBuilder(this).run {
            setView(dialog)
            create()
        }

        dialogView.apply {
            setCancelable(false)
            setOnShowListener {
                dialog.apply {
                    btnSetAdmin.setOnClickListener {
                        member.is_admin = Constans.ADMIN
                        member.toMap().let { it1 -> query1?.updateChildren(it1) }
                            ?.addOnSuccessListener {
                                context.toast("${member.email} is Admin Right now")
                            }
                        dialogView.dismiss()
                    }
                    btnRemoveAdmin.setOnClickListener {
                        member.is_admin = Constans.USER
                        member.toMap().let { it1 -> query1?.updateChildren(it1) }
                            ?.addOnSuccessListener {
                                context.toast("${member.email} is Removed from Admin")
                            }
                        dialogView.dismiss()
                    }
                    btnBack.setOnClickListener {
                        dialogView.dismiss()
                    }
                }
            }
        }
        dialogView.show()
    }

}