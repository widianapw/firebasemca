package com.example.firebasechat.presenter.chat

import com.example.firebasechat.data.chat.Chat
import com.example.firebasechat.data.handler.ErrorHandler
import com.example.firebasechat.data.handler.chatbot.ChatBotHandler
import com.example.firebasechat.presenter.BaseContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChatBotPresenter(val view: ChatBotContract.View, val view1: BaseContract.View) :
    ChatBotContract.Presenter, BaseContract.Presenter {
    val handler = ChatBotHandler()
    override fun sendMessage(data: HashMap<String, Any?>) {
        handler.sendMessage(data)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ErrorHandler<Chat>(this) {
                override fun onNext(t: Chat) {
                    view.sendResponse(t)
                }
            })
    }

    override fun showError(title: String, message: String) {
        view1.showError(title, message)
    }
}