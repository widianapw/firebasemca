package com.example.firebasechat.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.user.User
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.activity.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_group_create.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast

class ProfileFragment : Fragment() {
    private var mContext: Context? = null
    private val user = FirebaseAuth.getInstance().currentUser
    private val userDatabase =
        FirebaseDatabase.getInstance().getReference(Constans.USERS_TABLE_FIREBASE)
    var data: User? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val items = arrayListOf("Mahasiswa", "Dosen")
        val adapter = mContext?.let { ArrayAdapter(it, R.layout.list_item, items) }
        spinnerRoleUser?.setAdapter(adapter)
        val query = user?.uid?.let { userDatabase.child(it) }
        query?.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                data = snapshot.getValue(User::class.java)
                etEmailUser?.setText(data?.email)
                if (ivProfile != null) {
                    Glide.with(this@ProfileFragment)
                        .load(data?.photo_url)
                        .placeholder(R.color.colorPrimary).into(ivProfile)
                }
                etNimUser?.setText(data?.user_number)
                etNameUser?.setText(data?.name)
                spinnerRoleUser?.setText(data?.role, false)
            }
        })

        btnLogout?.setOnClickListener {
            Firebase.auth.signOut()
            activity?.finishAffinity()
            startActivity<LoginActivity>()
        }

        btnUpdateUser?.setOnClickListener {
            updateData()
        }
    }

    private fun updateData() {
        val name = etNameUser?.text.toString()
        val nim = etNimUser?.text.toString()
        val email = user?.email
        val role = spinnerRoleUser?.text.toString()
        data?.apply {
            this.name = name
            this.email = email
            this.user_number = nim
            this.role = role
        }
        user?.uid?.let {
            data?.toMap()?.let { it1 ->
                userDatabase.child(it).updateChildren(it1).addOnSuccessListener {
                    toast("Data Updated")
                }
            }
        }
    }
}