package com.example.firebasechat.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.data.user.User
import kotlinx.android.synthetic.main.item_user.view.*

class UserAdapter(var clickListener: (User?) -> Unit) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    var userList: List<User>? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = userList?.get(position)
        holder.itemView.apply {
            tvEmail?.text = userList?.get(position)?.email
            Glide.with(this).load(data?.photo_url).centerCrop().placeholder(R.color.colorPrimary)
                .into(imgUser)
            cardUser?.setOnClickListener {
                clickListener(data)
            }
        }
    }

    override fun getItemCount(): Int {
        return userList?.size ?: 0
    }

    fun setData(data: List<User>) {
        userList = data
        notifyDataSetChanged()
    }
}