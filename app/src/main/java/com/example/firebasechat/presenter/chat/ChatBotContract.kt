package com.example.firebasechat.presenter.chat

import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.chat.Chat
import io.reactivex.Observable
import retrofit2.http.*

interface ChatBotContract {
    interface View {
        fun sendResponse(response: Chat)
    }

    interface Presenter {
        fun sendMessage(data: HashMap<String, Any?>)
    }

    interface Handler {
        @FormUrlEncoded
        @POST(Constans.SEND_MESSAGE_URL)
        fun sendMessage(
            @FieldMap hashMap: HashMap<String, Any?>
        ): Observable<Chat>
    }

}