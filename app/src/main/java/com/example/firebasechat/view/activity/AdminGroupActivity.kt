package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.adapter.GroupAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_admin_group.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class AdminGroupActivity : AppCompatActivity() {
    val mUser = FirebaseAuth.getInstance().currentUser
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private val mAdapter = GroupAdapter {
        startActivity<GroupChatActivity>(Constans.GROUP_TABLE_FIREBASE to it)
    }
    val groupList = mutableListOf<Group>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_group)

        val type = intent.getIntExtra(Constans.ADMIN_FIELD, 10)
        toolbarGroup.setNavigationOnClickListener {
            onBackPressed()
        }

        rvGroupAdmin.apply {
            layoutManager = LinearLayoutManager(this@AdminGroupActivity)
            adapter = mAdapter
        }

        val query = mDatabase.child(Constans.GROUP_TABLE_FIREBASE)
        if (type == 0) {
            toolbarGroup.title = "Group Admin"
            query.addValueEventListener(object : ValueEventListenerAbstract() {
                override fun onDataChange(snapshot: DataSnapshot) {
                    groupList.clear()
                    for (i in snapshot.children) {
                        if (i.child(Constans.ADMIN_FIELD).value.toString() == mUser?.email) {
                            val data = i.getValue(Group::class.java)
                            Logger.addLogAdapter(AndroidLogAdapter())
                            Logger.d(data)
                            data?.let { groupList.add(it) }
                        }
                    }
                    mAdapter.setData(groupList)
                }
            })
        } else {
            val userGroup = mutableListOf<String>()
            toolbarGroup.title = "Group Non Admin"
            val userGroupQuery = mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE)
            userGroupQuery.addValueEventListener(object : ValueEventListenerAbstract() {
                override fun onDataChange(snapshot: DataSnapshot) {
                    userGroup.clear()
                    for (i in snapshot.children) {
                        if (mUser?.uid?.let { i.hasChild(it) }!!) {
                            userGroup.add(i.key.toString())
                        }
                    }
                    getNonAdminList(userGroup)
                }
            })
        }
    }

    private fun getNonAdminList(userGroup: MutableList<String>) {
        Logger.addLogAdapter(AndroidLogAdapter())
        Logger.d(userGroup)
        val query = mDatabase.child(Constans.GROUP_TABLE_FIREBASE)
        query.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                Logger.d(snapshot)
                groupList.clear()
                for (i in snapshot.children) {
                    if ((i.child(Constans.ADMIN_FIELD).value.toString() != mUser?.email) && (userGroup.contains(
                            i.key.toString()
                        ))
                    ) {
                        val data = i.getValue(Group::class.java)
                        data?.let { groupList.add(it) }
                    }
                }
                mAdapter.setData(groupList)
            }
        })

    }
}