package com.example.firebasechat.view

import android.util.Log
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

abstract class ValueEventListenerAbstract : ValueEventListener{
    override fun onCancelled(error: DatabaseError) {
       Log.d("onCancelled", error.message)
    }
}