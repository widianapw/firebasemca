package com.example.firebasechat.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.activity.AdminGroupActivity
import com.example.firebasechat.view.activity.GroupChatActivity
import com.example.firebasechat.view.activity.GroupCreateActivity
import com.example.firebasechat.view.activity.MainTabActivity
import com.example.firebasechat.view.adapter.GroupAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_group.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.yesButton


class GroupFragment : Fragment() {
    private var mContext: Context? = null
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private val mUser = FirebaseAuth.getInstance().currentUser
    private val userGroup = mutableListOf<String>()
    var groupList = mutableListOf<Group>()
    var mAdapter = GroupAdapter { group ->
        if (group.public_status?.toLowerCase() == "private") {
            alert("This Group Status is Private, Ask to Join?", "Group") {
                yesButton {
                    val data = HashMap<String, Any?>()
                    data[Constans.UID_FIELD] = mUser?.uid
                    data[Constans.EMAIL_FIELD] = mUser?.email
                    data[Constans.ALLOW_TYPING_FIELD] = Constans.ENABLE_TYPING
                    data[Constans.STATUS_FIELD] = Constans.STATUS_WAITING_APPROVED
                    data[Constans.IS_ADMIN_FIELD] = Constans.USER
                    group.gid?.let { it1 ->
                        mUser?.uid?.let { it2 ->
                            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it1)
                                .child(it2)
                                .setValue(data)
                                .addOnSuccessListener {
                                    toast("Wait For Approvement from Admin")
                                    svGroup?.setQuery("", false)
                                }
                        }
                    }
                }
                noButton { dialog ->
                    dialog.dismiss()
                }
            }.show()
        } else {
            alert("Join this Group?") {
                yesButton {
                    toast("joined")
                    val data = HashMap<String, Any?>()
                    data[Constans.UID_FIELD] = mUser?.uid
                    data[Constans.EMAIL_FIELD] = mUser?.email
                    data[Constans.ALLOW_TYPING_FIELD] = Constans.ENABLE_TYPING
                    data[Constans.STATUS_FIELD] = Constans.STATUS_APPROVED
                    data[Constans.IS_ADMIN_FIELD] = Constans.USER
                    group.gid?.let { it1 ->
                        mUser?.uid?.let { it2 ->
                            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it1)
                                .child(it2)
                                .setValue(data)
                                .addOnSuccessListener {
                                    toast("Joined")
                                    startActivity<GroupChatActivity>(Constans.GROUP_TABLE_FIREBASE to group)
                                }
                        }
                    }
                }
                noButton { dialog ->
                    dialog.dismiss()
                }
            }.show()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbarGroup.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_create -> {
                    startActivity<GroupCreateActivity>()
                    true
                }
                R.id.menu_group_admin -> {
                    startActivity<AdminGroupActivity>(Constans.ADMIN_FIELD to 0)
                    true
                }

                R.id.menu_group_not_admin -> {
                    startActivity<AdminGroupActivity>(Constans.ADMIN_FIELD to 1)
                    true
                }

                else -> false
            }
        }

        rvGroup?.apply {
            layoutManager = LinearLayoutManager(mContext)
            adapter = mAdapter
        }

        svGroup?.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query?.trim()?.isNotEmpty()!!)
                    searchGroup(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText?.trim().isNullOrEmpty()) {
                    groupList.clear()
                    mAdapter.setData(groupList)
                }
                return false
            }

        })


//        val userGroupQuery = mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE)
//        userGroupQuery.addValueEventListener(object : ValueEventListener {
//            override fun onDataChange(snapshot: DataSnapshot) {
//                userGroup.clear()
//                for (i in snapshot.children) {
//                    val uid = i.child(Constans.UID_FIELD).value.toString()
//                    if (mUser?.uid == uid) {
//                        userGroup.add(i.child(Constans.GROUP_ID_FIELD).value.toString())
//                    }
//                }
//                groupList(userGroup)
//            }
//
//            override fun onCancelled(error: DatabaseError) {
//                Log.d("onCancelled", error.message)
//            }
//
//        })

    }

////    private fun groupList(userGroup: MutableList<String>) {
////        val query = mDatabase.child(Constans.GROUP_TABLE_FIREBASE)
////        query.addValueEventListener(object : ValueEventListener {
////            override fun onDataChange(snapshot: DataSnapshot) {
////                groupList.clear()
////                for (i in snapshot.children) {
////                    if ((i.child(Constans.ADMIN_FIELD).value.toString() != mUser?.email) && (!userGroup.contains(
////                            i.key.toString()
////                        ))
////                    ) {
////                        val data = Group(
////                            i.key,
////                            name = i.child(Constans.NAME_FIELD).value.toString(),
////                            admin = i.child(Constans.ADMIN_FIELD).value.toString(),
////                            allowTyping = i.child(Constans.ALLOW_TYPING_FIELD).value.toString(),
////                            publicStatus = i.child(Constans.PUBLIC_STATUS_FIELD).value.toString()
////                        )
////                        groupList.add(data)
////                    }
////                }
////                mAdapter.setData(groupList)
////            }
////
////            override fun onCancelled(error: DatabaseError) {
////                Log.d("onCancelled", error.message)
////            }
////        })
//
//    }


    fun searchGroup(groupId: String) {
        val query =
            mDatabase.child(Constans.GROUP_TABLE_FIREBASE).orderByChild(Constans.GROUP_CODE)
                .equalTo(groupId.trim())
        query.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                groupList.clear()
                for (i in snapshot.children) {
                    val data = Group(
                        i.child(Constans.GROUP_ID_FIELD).value.toString(),
                        i.child(Constans.GROUP_CODE).value.toString(),
                        i.child(Constans.NAME_FIELD).value.toString(),
                        i.child(Constans.ADMIN_FIELD).value.toString(),
                        i.child(Constans.PUBLIC_STATUS_FIELD).value.toString(),
                        i.child(Constans.ALLOW_TYPING_FIELD).value.toString()
                    )
                    if (data.admin != mUser?.email)
                        groupList.add(data)
                }
                mAdapter.setData(groupList)
            }
        })
    }

}

