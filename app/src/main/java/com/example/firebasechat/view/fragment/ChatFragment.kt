package com.example.firebasechat.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.chat.Chat
import com.example.firebasechat.presenter.BaseContract
import com.example.firebasechat.presenter.chat.ChatBotContract
import com.example.firebasechat.presenter.chat.ChatBotPresenter
import com.example.firebasechat.view.adapter.ChatAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_chat.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton
import kotlin.collections.HashMap

class ChatFragment : Fragment(), ChatBotContract.View, BaseContract.View {
    var chatList = mutableListOf<Chat>()
    private val user = FirebaseAuth.getInstance().currentUser
    private val mDatabase =
        FirebaseDatabase.getInstance().reference
    private val outBoxTable =
        FirebaseDatabase.getInstance().getReference(Constans.OUTBOX_TABLE_FIREBASE)

    private val mAdapter = ChatAdapter()
    private var mContext: Context? = null

    private lateinit var presenter: ChatBotPresenter
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ChatBotPresenter(this, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarChat.title = "Bot Auto Reply"
        val recycler = rvChat
        val mLayoutManager = LinearLayoutManager(mContext)
        mLayoutManager.stackFromEnd = true
        recycler.apply {
            layoutManager = mLayoutManager
            adapter = mAdapter
        }
        btnSendChat.setOnClickListener {
            val message = etMessageTalk?.text.toString().trim()
            if (message.isNotEmpty()) {
                sendMessage(message)
            }
        }

        //observe chat
        val query =
            mDatabase.child(Constans.CHAT_TABLE_FIREBASE).orderByChild(Constans.TIMESTAMP_FIELD)

        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                for (i in snapshot.children) {
                    if (i.child(Constans.UID_FIELD).value.toString() == user?.uid) {
                        val data = Chat(
                            "0",
                            i.child(Constans.UID_FIELD).value.toString(),
                            i.child(Constans.EMAIL_FIELD).value.toString(),
                            "0",
                            i.child(Constans.UID_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_FIELD).value.toString(),
                            i.child(Constans.TIMESTAMP_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_TYPE_FIELD).value.toString().toInt()
                        )
                        chatList.add(data)
                    }
                }
                Logger.addLogAdapter(AndroidLogAdapter())
                Logger.d(chatList)
                chatList.toList().let { mAdapter.setData(it) }
                recycler.scrollToPosition(mAdapter.itemCount - 1)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("onCancelled", error.message)
            }
        })
    }


    private fun sendMessage(message: String) {
        val timeStamp = System.currentTimeMillis().toString()
        val send = HashMap<String, Any?>()
        send[Constans.UID_FIELD] = user?.uid
        send[Constans.EMAIL_FIELD] = user?.email
        send[Constans.MESSAGE_FIELD] = message
        send[Constans.TIMESTAMP_FIELD] = timeStamp
        send[Constans.MESSAGE_TYPE_FIELD] = Constans.MSG_SELF
        Logger.d(send)
        presenter.sendMessage(send)
        etMessageTalk?.setText("")
    }

    override fun sendResponse(response: Chat) {
        Logger.d(response.toMap())
        val send = HashMap<String, Any?>()
        send[Constans.UID_FIELD] = response.uid
        send[Constans.EMAIL_FIELD] = response.email
        send[Constans.MESSAGE_FIELD] = response.message
        send[Constans.TIMESTAMP_FIELD] = response.timestamp
        send[Constans.MESSAGE_TYPE_FIELD] = response.messageType
        mDatabase.apply {
            child(Constans.CHAT_TABLE_FIREBASE).push().setValue(send)
                .addOnSuccessListener {
                    val timeStamp1 = System.currentTimeMillis().toString()
                    response.timestamp = timeStamp1
                    child(Constans.INBOX_TABLE_FIREBASE).push().setValue(send)
                }
        }
    }

    override fun showError(title: String, message: String) {
        alert(message, title) {
            yesButton {
                it.dismiss()
            }
        }.show()
    }
}