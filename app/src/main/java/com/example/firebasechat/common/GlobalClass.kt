package com.example.firebasechat.common

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate

class GlobalClass : Application(){
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}