package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import kotlinx.android.synthetic.main.activity_photo_viewer.*

class PhotoViewerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_viewer)
        toolbarPhotoViewer.setNavigationOnClickListener {
            onBackPressed()
        }

        Glide.with(this).load(intent?.getStringExtra(Constans.IMG_PATH))
            .placeholder(R.color.colorWhite).into(imgPhotoViewer)
    }
}