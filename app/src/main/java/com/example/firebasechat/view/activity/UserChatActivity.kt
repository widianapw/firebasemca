package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.chat.Chat
import com.example.firebasechat.data.user.User
import com.example.firebasechat.view.adapter.ChatAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_chat.*

class UserChatActivity : AppCompatActivity() {
    var chatKey = ""
    private val user = FirebaseAuth.getInstance().currentUser
    private val mDatabase =
        FirebaseDatabase.getInstance().reference
    private val db =
        FirebaseDatabase.getInstance()
    var chatList = mutableListOf<Chat>()
    val mAdapter = ChatAdapter()
    var friendUid = ""
    var friend : User?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_chat)
        val mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.stackFromEnd = true
        rvChat.apply {
            layoutManager = mLayoutManager
            adapter = mAdapter
        }

        friend = intent.getParcelableExtra(Constans.USERS_TABLE_FIREBASE)
        friendUid = friend?.uid?:""
        toolbarChat.title = friend?.email?:""
        val query = mDatabase.child(Constans.CHAT_ROOM_TABLE_FIREBASE)
        val queryChat = mDatabase.child(Constans.CHAT_USER_TABLE_FIREBASE)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (i in snapshot.children) {
                    if ((i.child(Constans.UID1_FIELD).value.toString() == user?.uid || i.child(
                            Constans.UID2_FIELD
                        ).value.toString() == user?.uid) && (i.child(Constans.UID1_FIELD).value.toString() == friendUid || i.child(
                            Constans.UID2_FIELD
                        ).value.toString() == friendUid)
                    ) {
                        chatKey = i?.key ?: ""
                        Log.d("chatKey", chatKey)
                    }
                }
                if (chatKey.isEmpty()) {
                    val chatRoom = HashMap<String, Any?>()
                    chatRoom[Constans.UID1_FIELD] = user?.uid
                    chatRoom[Constans.UID2_FIELD] = friendUid
                    user?.uid?.let {
                        db.getReference(Constans.CHAT_ROOM_TABLE_FIREBASE).push().setValue(chatRoom)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("onCancelled", error.message)
            }

        })


        queryChat.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                for (i in snapshot.children) {
                    if (i.child(Constans.CHAT_ROOM_FIELD).value.toString() == chatKey) {
                        val data = Chat(
                            i.child(Constans.CHAT_ROOM_FIELD).value.toString(),
                            i.child(Constans.UID_FIELD).value.toString(),
                            i.child(Constans.EMAIL_FIELD).value.toString(),
                            i.child(Constans.UID1_FIELD).value.toString(),
                            i.child(Constans.UID2_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_FIELD).value.toString(),
                            i.child(Constans.TIMESTAMP_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_TYPE_FIELD).value.toString().toInt()
                        )
                        chatList.add(data)
                    }
                }
                mAdapter.setData(chatList)
                rvChat.scrollToPosition(mAdapter.itemCount - 1)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("onCancelled", error.message)
            }

        })

        btnSendChat.setOnClickListener {
            val message = etMessageTalk.text.toString().trim()
            if (!message.isNullOrEmpty())
                sendChat(message)
        }

    }


    private fun sendChat(message: String) {
        val timeStamp = System.currentTimeMillis().toString()
        val send = HashMap<String, Any?>()
        send[Constans.CHAT_ROOM_FIELD] = chatKey
        send[Constans.UID_FIELD] = user?.uid
        send[Constans.EMAIL_FIELD] = user?.email
        send[Constans.UID1_FIELD] = user?.uid
        send[Constans.UID2_FIELD] = friendUid
        send[Constans.MESSAGE_FIELD] = message
        send[Constans.TIMESTAMP_FIELD] = timeStamp
        send[Constans.MESSAGE_TYPE_FIELD] = Constans.MSG_SELF
        mDatabase.apply {
            child(Constans.CHAT_USER_TABLE_FIREBASE).push().setValue(send).addOnSuccessListener {
                val timeStamp1 = System.currentTimeMillis().toString()
                send[Constans.TIMESTAMP_FIELD] = timeStamp1
//                child(Constans.INBOX_TABLE_FIREBASE).push().setValue(send)
            }
        }
        etMessageTalk?.setText("")
    }

}