package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.example.firebasechat.R
import com.example.firebasechat.view.fragment.ChatFragment
import com.example.firebasechat.view.fragment.GroupFragment
import com.example.firebasechat.view.fragment.ProfileFragment
import com.example.firebasechat.view.fragment.UserFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main_tab.*

class MainTabActivity : AppCompatActivity() {

    private val navigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.menu_user -> {
                if (getDisplayedFragment().javaClass.simpleName != "UserFragment")
                    addFragment(UserFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_group -> {
                if (getDisplayedFragment().javaClass.simpleName != "GroupFragment")
                    addStackFragment(GroupFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_profile -> {
                if (getDisplayedFragment().javaClass.simpleName != "ProfileFragment")
                    addStackFragment(ProfileFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_chat -> {
                if (getDisplayedFragment().javaClass.simpleName != "ChatFragment")
                    addStackFragment(ChatFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tab)
        bottomNavigationMain?.setOnNavigationItemSelectedListener(navigationListener)
        if (savedInstanceState == null) {
            val fragment = UserFragment()
            addFragment(fragment)
        }
        Log.d("DISPLAYED FRAGMENT", getDisplayedFragment().javaClass.simpleName)
    }

    private fun getDisplayedFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.fragment_content) ?: UserFragment()
    }

    private fun addFragment(fragment: Fragment) {
        if (supportFragmentManager.backStackEntryCount != 0)
            supportFragmentManager
                .popBackStack()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    private fun addStackFragment(fragment: Fragment) {
        Log.d("displayedFragment", getDisplayedFragment().javaClass.simpleName)
        Log.d("selectedFragment", fragment.javaClass.simpleName)
        if (getDisplayedFragment().javaClass.simpleName != "UserFragment") {

            supportFragmentManager
                .popBackStack()

            supportFragmentManager
                .beginTransaction()
                .remove(getDisplayedFragment())
                .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
        } else {

            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
        }

    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        Log.d("COunt Back", count.toString() + "  " + bottomNavigationMain.selectedItemId)
        if (count != 0) {
            bottomNavigationMain.menu.findItem(R.id.menu_user).isChecked = true
        }
        super.onBackPressed()
    }
}