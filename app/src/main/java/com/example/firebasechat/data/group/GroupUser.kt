package com.example.firebasechat.data.group

import android.os.Parcelable
import com.example.firebasechat.common.Constans
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GroupUser(
    var uid: String? = null,
    var email: String? = null,
    var allow_typing: String? = null,
    var status: String? = null,
    var is_admin: String? = null
) : Parcelable {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            Constans.UID_FIELD to uid,
            Constans.EMAIL_FIELD to email,
            Constans.ALLOW_TYPING_FIELD to allow_typing,
            Constans.STATUS_FIELD to status,
            Constans.IS_ADMIN_FIELD to is_admin
        )
    }
}