package com.example.firebasechat.presenter

interface BaseContract {
    interface View {
        fun showError(title: String, message: String)
    }

    interface Presenter {
        fun showError(title: String, message: String)
    }
}