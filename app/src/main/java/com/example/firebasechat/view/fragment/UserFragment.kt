package com.example.firebasechat.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.user.User
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.activity.UserChatActivity
import com.example.firebasechat.view.adapter.UserAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_user.*
import org.jetbrains.anko.Android
import org.jetbrains.anko.support.v4.startActivity

class UserFragment : Fragment() {
    private val user = FirebaseAuth.getInstance().currentUser
    private val mDatabase =
        FirebaseDatabase.getInstance().reference
    private val userList = mutableListOf<User>()
    private var mContext: Context? = null
    var mAdapter = UserAdapter {
        startActivity<UserChatActivity>(
            Constans.USERS_TABLE_FIREBASE to it
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        rvUser.apply {
            layoutManager = LinearLayoutManager(mContext)
            adapter = mAdapter
        }

        val userQuery = mDatabase.child(Constans.USERS_TABLE_FIREBASE)
        userQuery.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
                for (i in snapshot.children) {
                    if (i.child(Constans.UID_FIELD).value.toString() != user?.uid) {
                        val data = i.getValue(User::class.java)
                        data?.let { userList.add(it) }
                    }
                }
                Logger.addLogAdapter(AndroidLogAdapter())
                Logger.d(userList)
                mAdapter.setData(userList)
            }
        })

    }


}