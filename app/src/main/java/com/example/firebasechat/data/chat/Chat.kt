package com.example.firebasechat.data.chat

import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.BaseResultData
import com.google.gson.annotations.Expose
import java.sql.Timestamp

data class Chat(
    @Expose
    var chatRoom: String? = null,
    @Expose
    var uid: String? = null,
    @Expose
    var email: String? = null,
    @Expose
    var fromUid: String? = null,
    @Expose
    var toUid: String? = null,
    @Expose
    var message: String? = null,
    @Expose
    var timestamp: String? = null,
    @Expose
    var messageType: Int? = 1
) : BaseResultData() {
    fun toMap(): Map<String, Any?> {
        return mapOf<String, Any?>(
            Constans.CHAT_ROOM_FIELD to chatRoom,
            Constans.UID_FIELD to uid,
            Constans.EMAIL_FIELD to email,
            Constans.FROM_UID_FIELD to fromUid,
            Constans.TO_UID_FIELD to toUid,
            Constans.MESSAGE_FIELD to message,
            Constans.TIMESTAMP_FIELD to timestamp,
            Constans.MESSAGE_TYPE_FIELD to messageType
        )
    }
}