package com.example.firebasechat.data.group

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.sql.Timestamp

@Parcelize
data class GroupChat(
    var uid: String?=null,
    var email: String?=null,
    var message: String?=null,
    var timestamp: String?=null,
    var file_name: String?=null,
    var message_type: String? =null
) : Parcelable