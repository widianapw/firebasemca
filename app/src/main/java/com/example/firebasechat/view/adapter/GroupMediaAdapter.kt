package com.example.firebasechat.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.GroupChat
import kotlinx.android.synthetic.main.item_image_group.view.*

class GroupMediaAdapter(val clickListener: (GroupChat) -> Unit) :
    RecyclerView.Adapter<GroupMediaAdapter.ViewHolder>() {
    private var mediaList: List<GroupChat>? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_image_group, parent, false)
        )
//        return when (viewType) {
//            Constans.IMAGE_FILE_TYPE.toInt() -> {
//                ViewHolder(
//                    LayoutInflater.from(parent.context)
//                        .inflate(R.layout.item_image_group, parent, false)
//                )
//            }
//            Constans.FILE_FILE_TYPE.toInt() -> {
//                ViewHolder(
//                    LayoutInflater.from(parent.context)
//                        .inflate(R.layout.item_image_group, parent, false)
//                )
//            }
//            else -> ViewHolder(
//                LayoutInflater.from(parent.context)
//                    .inflate(R.layout.item_image_group, parent, false)
//            )
//        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val media = mediaList?.get(position)
        holder.itemView.apply {
            if (media?.message_type == Constans.MESSAGE_TYPE_FILE) {
                Glide.with(this).load(R.drawable.ic_baseline_insert_drive_file_24).into(ivMedia)
            } else {
                Glide.with(this).load(media?.message).placeholder(R.color.colorPrimary)
                    .into(ivMedia)
            }
            tvMediaName?.text = media?.file_name
            cardMedia?.setOnClickListener {
                media?.let { it1 -> clickListener(it1) }
            }
        }
    }

    override fun getItemCount(): Int {
        return mediaList?.count() ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return when (mediaList?.get(position)?.message_type) {
            Constans.MESSAGE_TYPE_IMG -> Constans.MESSAGE_TYPE_IMG.toInt()
            Constans.MESSAGE_TYPE_FILE -> Constans.MESSAGE_TYPE_FILE.toInt()
            else -> 0
        }
    }

    fun setData(data: List<GroupChat>) {
        mediaList = data
        notifyDataSetChanged()
    }
}