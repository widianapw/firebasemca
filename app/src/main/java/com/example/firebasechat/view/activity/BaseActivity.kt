package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

open class BaseActivity: AppCompatActivity() {
    var mDatabase = FirebaseDatabase.getInstance().reference
    var mUser = FirebaseAuth.getInstance().currentUser

    var userId = mUser?.uid
    var userEmail = mUser?.email
}