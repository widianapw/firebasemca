package com.example.firebasechat.view.activity

import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.common.Utilities
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.data.group.GroupChat
import com.example.firebasechat.data.group.GroupUser
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.example.firebasechat.view.adapter.GroupChatAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.storage.FirebaseStorage
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_group_chat.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class GroupChatActivity : BaseActivity() {
    var messageList = mutableListOf<GroupChat>()
    lateinit var mAdapter: GroupChatAdapter
    var mUserGroup: GroupUser? = null
    var myDownloadDid: Long = 0
    private val storageReference = FirebaseStorage.getInstance().reference
    override fun onCreate(savedInstanceState: Bundle?) {
        val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_chat)
        Logger.addLogAdapter(AndroidLogAdapter())
        Logger.d(group)
        mAdapter = GroupChatAdapter {
            checkWritePermission(it)
        }
        initView()
        val query = group?.gid?.let {
            mUser?.uid?.let { it1 ->
                mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it).child(
                    it1
                )
            }
        }
        query?.addListenerForSingleValueEvent(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                mUserGroup = snapshot.getValue(GroupUser::class.java)
                listenChat(mUserGroup)
            }
        })
        listenDataChanges()
        btnAddAttachment?.setOnClickListener {
            if (cardAttachment.visibility == View.VISIBLE) {
                cardAttachment?.visibility = View.GONE
            } else {
                cardAttachment?.visibility = View.VISIBLE
            }
        }

        groupImageChat.setOnClickListener {
            checkCameraPermission()
        }
        groupFileChat.setOnClickListener {
            checkFilePermission()
        }
        val br = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
                if (id == myDownloadDid) {
                    context?.toast("Download Completed")
                }
            }
        }

        registerReceiver(br, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }

    private fun downloadFile(it: GroupChat) {

//                val intent = Intent(Intent.ACTION_VIEW, chat?.message?.toUri())
//                context.startActivity(intent)
        toast("Downloading File...")
        val fileUri = DownloadManager.Request(Uri.parse(it.message))
            .setTitle("Download File")
            .setDescription("Downloading File...")
        fileUri.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            it.file_name
        )
        fileUri.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
        myDownloadDid = dm?.enqueue(fileUri)!!
    }


    private fun initView() {
        val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        toolbarGroupChat?.apply {
            title = group?.name
            setNavigationOnClickListener {
                onBackPressed()
            }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menu_member -> {
                        startActivity<GroupMemberActivity>(Constans.GROUP_TABLE_FIREBASE to group)
                        true
                    }
                    R.id.menu_group_setting -> {
                        startActivity<GroupSettingActivity>(Constans.GROUP_TABLE_FIREBASE to group)
                        true
                    }
                    R.id.menu_file -> {
                        startActivity<GroupMediaActivity>(
                            Constans.MESSAGE_TYPE_FIELD to Constans.MESSAGE_TYPE_FILE,
                            Constans.GROUP_TABLE_FIREBASE to group
                        )
                        true
                    }
                    R.id.menu_image -> {
                        startActivity<GroupMediaActivity>(
                            Constans.MESSAGE_TYPE_FIELD to Constans.MESSAGE_TYPE_IMG,
                            Constans.GROUP_TABLE_FIREBASE to group
                        )
                        true
                    }
                    else -> false
                }
            }
            menu.findItem(R.id.menu_group_setting).isVisible = group?.admin == mUser?.email
        }

        btnSendChat?.setOnClickListener {
            val timeStamp = System.currentTimeMillis().toString()
            val message = etMessageTalk?.text.toString().trim()

            if (message.isNotEmpty()) {
                val data = HashMap<String, Any?>()
                data[Constans.UID_FIELD] = mUser?.uid
                data[Constans.EMAIL_FIELD] = mUser?.email
                data[Constans.MESSAGE_FIELD] = message
                data[Constans.TIMESTAMP_FIELD] = timeStamp
                data[Constans.FILE_NAME_FIELD] = ""
                data[Constans.MESSAGE_TYPE_FIELD] = Constans.MESSAGE_TYPE_CHAT
                group?.gid?.let { it1 ->
                    mDatabase.child(Constans.CHAT_GROUP_TABLE_FIREBASE).child(
                        it1
                    ).push().setValue(data)
                }
                etMessageTalk.setText("")
            }
        }

        val mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.stackFromEnd = true
        rvChat.apply {
            layoutManager = mLayoutManager
            adapter = mAdapter
        }
    }

    private fun listenChat(mUserGroup: GroupUser?) {
        Logger.d(mUserGroup)
        if (mUserGroup?.status != Constans.STATUS_WAITING_APPROVED) {
            val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
            val query =
                group?.gid?.let { mDatabase.child(Constans.CHAT_GROUP_TABLE_FIREBASE).child(it) }
            query?.addValueEventListener(object : ValueEventListenerAbstract() {
                override fun onDataChange(snapshot: DataSnapshot) {
                    messageList.clear()
                    for (i in snapshot.children) {
                        val chat = GroupChat(
                            i.child(Constans.UID_FIELD).value.toString(),
                            i.child(Constans.EMAIL_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_FIELD).value.toString(),
                            i.child(Constans.TIMESTAMP_FIELD).value.toString(),
                            i.child(Constans.FILE_NAME_FIELD).value.toString(),
                            i.child(Constans.MESSAGE_TYPE_FIELD).value.toString()
                        )
                        messageList.add(chat)
                    }
                    Logger.addLogAdapter(AndroidLogAdapter())
                    Logger.d(messageList)
                    mAdapter.setData(messageList)
                    rvChat.scrollToPosition(mAdapter.itemCount - 1)
                }
            })

        }
    }

    private fun listenDataChanges() {
        val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        val query = group?.gid?.let {
            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(it)
                .orderByChild(Constans.UID_FIELD).equalTo(mUser?.uid)
        }
        query?.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                Logger.d(snapshot)
                for (i in snapshot.children) {
                    if (i.child(Constans.ALLOW_TYPING_FIELD).value.toString() == Constans.DISABLE_TYPING) {
                        toast("Typing was disabled by admin")
                        etMessageTalk?.isEnabled = false
                        btnSendChat?.isEnabled = false
                        btnAddAttachment?.isEnabled = false
                        cardAttachment?.visibility = View.GONE
                    } else {
                        toast("Typing was enabled by admin")
                        etMessageTalk?.isEnabled = true
                        btnSendChat?.isEnabled = true
                        btnAddAttachment?.isEnabled = true
                    }
                }
            }
        })

    }

    private fun openImageResources() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setAspectRatio(1, 1)
            .start(this)
    }

    private fun checkCameraPermission() {
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)) {
            PermissionChecker.PERMISSION_GRANTED -> {
                openImageResources()
            }
            PermissionChecker.PERMISSION_DENIED -> {
                //Request Location Permission
                Utilities.checkCameraPermission(this)
            }
        }
    }

    private fun checkFilePermission() {
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            PermissionChecker.PERMISSION_GRANTED -> {
                val mimeTypes = arrayOf(
                    "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // .doc & .docx
                    "application/vnd.ms-powerpoint",
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation",  // .ppt & .pptx
                    "application/vnd.ms-excel",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  // .xls & .xlsx
                    "text/plain",
                    "application/pdf",
                    "application/zip"
                )
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = if (mimeTypes.count() == 1) mimeTypes[0] else "*/*"
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, Constans.MY_PERMISSIONS_REQUEST_FILE)
            }
            PermissionChecker.PERMISSION_DENIED -> {
                //Request Location Permission
                Utilities.checkFilePermission(this)
            }
        }
    }

    private fun checkWritePermission(it: GroupChat) {
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionChecker.PERMISSION_GRANTED -> {
                downloadFile(it)
            }
            PermissionChecker.PERMISSION_DENIED -> {
                //Request Location Permission
                Utilities.checkWritePermission(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            cardAttachment.visibility = View.GONE
            toast("Uploading...")
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                val result = CropImage.getActivityResult(data)
                if (resultCode == Activity.RESULT_OK) {
                    val resultUri = result.uri
                    uploadFile(resultUri, Constans.IMAGE_FILE_TYPE)
                } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    val error = result.error
                }
            } else if (requestCode == Constans.MY_PERMISSIONS_REQUEST_FILE) {
                val result = data?.data
                uploadFile(result!!, Constans.FILE_FILE_TYPE)
            }
        }
    }

    private fun uploadFile(uri: Uri, fileType: String) {
        Log.d("File Size", getFileSize(uri) + " / " + getFileName(uri))
        if (fileType == Constans.FILE_FILE_TYPE && getFileSize(uri).toFloat() / 1000 > 2000) {
            alert("Maximum file size is 2MB", "Maximum File Size Exceeded") {
                yesButton {
                    it.dismiss()
                }
                isCancelable = false
            }.show()
        } else {
            val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
            val timeStamp = System.currentTimeMillis().toString()
            val storageReference2 = if (fileType == Constans.IMAGE_FILE_TYPE) {
                val filePath = group?.gid + "/" + Constans.IMG_PATH + timeStamp
                storageReference.child(filePath)
            } else {
                val filePath = group?.gid + "/" + Constans.FILE_PATH + timeStamp
                storageReference.child(filePath)
            }
            val fileName = getFileName(uri)

            storageReference2.putFile(uri)
                .addOnSuccessListener {
                    val uriTask = it.storage.downloadUrl
                    while (!uriTask.isSuccessful) {

                    }
                    val downloadUri = uriTask.result
                    Log.d("posisi", "uri task = " + uriTask.isSuccessful)
                    if (uriTask.isSuccessful) {
                        toast("Uploaded")
                        val data = HashMap<String, Any?>()
                        data[Constans.UID_FIELD] = mUser?.uid
                        data[Constans.EMAIL_FIELD] = mUser?.email
                        data[Constans.MESSAGE_FIELD] = downloadUri.toString()
                        data[Constans.TIMESTAMP_FIELD] = timeStamp
                        data[Constans.FILE_NAME_FIELD] = fileName
                        if (fileType == Constans.IMAGE_FILE_TYPE)
                            data[Constans.MESSAGE_TYPE_FIELD] = Constans.MESSAGE_TYPE_IMG
                        else
                            data[Constans.MESSAGE_TYPE_FIELD] = Constans.MESSAGE_TYPE_FILE
                        group?.gid?.let { it1 ->
                            mDatabase.child(Constans.CHAT_GROUP_TABLE_FIREBASE).child(
                                it1
                            ).push().setValue(data)
                        }

                    }
                }
        }
    }

    private fun getFileName(uri: Uri): String {
        var name = ""
        uri.let { returnUri ->
            contentResolver.query(returnUri, null, null, null, null)
        }?.use { cursor ->
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            cursor.moveToFirst()
            name = cursor.getString(nameIndex)
        }
        return name
    }

    fun getFileSize(uri: Uri): String {
        var size = ""
        uri.let { returnUri ->
            contentResolver.query(returnUri, null, null, null, null)
        }?.use { cursor ->
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
            cursor.moveToFirst()
            size = cursor.getString(sizeIndex)
        }
        return size
    }

    override fun onBackPressed() {
        if (cardAttachment?.visibility == View.VISIBLE)
            cardAttachment?.visibility = View.GONE
        else
            super.onBackPressed()
    }
}