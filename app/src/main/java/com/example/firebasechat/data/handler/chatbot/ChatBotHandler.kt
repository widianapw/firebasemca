package com.example.firebasechat.data.handler.chatbot

import com.example.firebasechat.data.chat.Chat
import com.example.firebasechat.data.handler.BaseHandler
import com.example.firebasechat.presenter.chat.ChatBotContract
import io.reactivex.Observable

class ChatBotHandler : BaseHandler() {
    private val service = getClient().create(ChatBotContract.Handler::class.java)
    fun sendMessage(data: HashMap<String, Any?>): Observable<Chat> {
        return service.sendMessage(data)
    }
}