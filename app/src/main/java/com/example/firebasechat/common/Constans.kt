package com.example.firebasechat.common

import java.util.regex.Pattern

object Constans {

//    const val BASE_URL = "https://mca.widianapw.com/api/"
    const val BASE_URL = "https://b266bf74c938.ngrok.io/api/"
    const val SEND_MESSAGE_URL = BASE_URL+"sendChat"

    const val SHARED_PREFERENCES = "shared_preferences"
    const val MSG_SELF = 1
    const val MSG_ENGINE = 2
    const val ENABLE_TYPING = "0"
    const val DISABLE_TYPING = "1"
    const val STATUS_APPROVED = "1"
    const val STATUS_WAITING_APPROVED = "0"
    const val ADMIN = "1"
    const val SUPER_ADMIN = "2"
    const val USER = "0"
    val EMAIL_FIREBASE = "email"
    val UID_FIREBASE = "uid"
    val NAME_FIREBASE = "name"
    val PHONE_FIREBASE = "phone"
    val IMAGE_FIREBASE = "image"
    val COVER_FIREBASE = "cover"

    const val USERS_TABLE_FIREBASE = "users"

    const val INBOX_TABLE_FIREBASE = "inbox"
    const val OUTBOX_TABLE_FIREBASE = "outbox"
    const val CHAT_ROOM_TABLE_FIREBASE = "chat_room"
    const val CHAT_TABLE_FIREBASE = "chat"
    const val CHAT_USER_TABLE_FIREBASE = "chat_user"
    const val GROUP_TABLE_FIREBASE = "group"
    const val GROUP_USER_TABLE_FIREBASE = "group_user"
    const val CHAT_GROUP_TABLE_FIREBASE = "chat_group"
    const val RC_SIGN_IN = 100

    const val UID_FIELD = "uid"
    const val FROM_UID_FIELD = "uid"
    const val TO_UID_FIELD = "uid"
    const val CHAT_ROOM_FIELD = "chat_room"
    const val MESSAGE_FIELD = "message"
    const val TIMESTAMP_FIELD = "timestamp"
    const val MESSAGE_TYPE_FIELD = "message_type"
    const val EMAIL_FIELD = "email"
    const val PHOTO_URL = "photo_url"
    const val UID1_FIELD = "from_uid"
    const val UID2_FIELD = "to_uid"
    const val STATUS_FIELD = "status"
    const val GROUP_ID_FIELD = "gid"
    const val GROUP_USER_ID_FIELD = "guid"
    const val GROUP_CODE = "group_code"

    const val NAME_FIELD = "name"
    const val ROLE_FIELD = "role"
    const val USER_NUMBER_FIELD = "user_number"
    const val ADMIN_FIELD = "admin"
    const val PUBLIC_STATUS_FIELD = "public_status"
    const val ALLOW_TYPING_FIELD = "allow_typing"
    const val IS_ADMIN_FIELD = "is_admin"
    const val FILE_NAME_FIELD = "file_name"


    //premission
    const val MY_PERMISSIONS_REQUEST_CAMERA = 322
    const val MY_PERMISSIONS_REQUEST_FILE = 344
    const val MY_PERMISSIONS_WRITE_FILE = 345


    const val IMG_PATH = "chat_img/"
    const val FILE_PATH = "chat_file/"

    const val IMAGE_FILE_TYPE = "image"
    const val FILE_FILE_TYPE = "file"

    const val MESSAGE_TYPE_CHAT = "0"
    const val MESSAGE_TYPE_IMG = "1"
    const val MESSAGE_TYPE_FILE = "2"

}