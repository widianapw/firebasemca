package com.example.firebasechat.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.data.group.GroupUser
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.item_group_user.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class GroupUserAdapter(
    private val isAdmin: String,
    val group: Group,
    val clickListener: (GroupUser)->Unit
) :
    RecyclerView.Adapter<GroupUserAdapter.ViewHolder>() {
    var memberList: List<GroupUser>? = null
    var mUser = FirebaseAuth.getInstance().currentUser
    var mDatabase = FirebaseDatabase.getInstance().reference

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_group_user, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val member = memberList?.get(position)
        holder.itemView.apply {
            tvEmail.text = member?.email.toString()
            when(member?.is_admin){
                Constans.ADMIN -> tvStatus?.text = "Admin"
                Constans.USER -> tvStatus?.text = "User"
                Constans.SUPER_ADMIN -> tvStatus?.text = "Super Admin"
            }
            cardUser.setOnClickListener {
                member?.let { it1 -> clickListener(it1) }
            }
            if (isAdmin == Constans.SUPER_ADMIN || isAdmin == Constans.ADMIN) {
                if (member?.uid == mUser?.uid || member?.is_admin == Constans.SUPER_ADMIN || member?.is_admin == Constans.ADMIN) {
                    btnDisableTyping.setVisibility(false)
                    btnEnableTyping.setVisibility(false)
                    btnApprove.setVisibility(false)
                    btnKick.setVisibility(false)
                } else {
                    if (member?.status == Constans.STATUS_APPROVED) {
                        //1=joined
                        if (member.allow_typing == Constans.ENABLE_TYPING) {
                            // 0 = true
                            btnDisableTyping.setVisibility(true)
                            btnKick.setVisibility(true)

                            btnEnableTyping.setVisibility(false)
                            btnApprove.setVisibility(false)
                        } else {
                            btnDisableTyping.setVisibility(false)
                            btnKick.setVisibility(true)

                            btnEnableTyping.setVisibility(true)
                            btnApprove.setVisibility(false)
                        }
                    } else if (member?.status == Constans.STATUS_WAITING_APPROVED) {
                        //0=wait approvement
                        btnApprove.setVisibility(true)
                        btnKick.setVisibility(true)

                        btnDisableTyping.setVisibility(false)
                        btnEnableTyping.setVisibility(false)
                    }
                }
            }


            btnKick.setOnClickListener {
                context.alert("Are you sure?") {
                    yesButton {
                        val query1 =
                            group.gid?.let { it1 ->
                                FirebaseDatabase.getInstance().reference.child(Constans.GROUP_USER_TABLE_FIREBASE)
                                    .child(
                                        it1
                                    ).orderByChild(Constans.UID_FIELD).equalTo(member?.uid)
                            }

                        query1?.addValueEventListener(object : ValueEventListenerAbstract() {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                for (i in snapshot.children) {
                                    i.ref.removeValue()
                                }
                            }
                        })
                    }
                    noButton {
                        it.dismiss()
                    }
                }.show()
            }

            btnDisableTyping.setOnClickListener {
                val query1 =
                    group.gid?.let { it1 ->
                        member?.uid?.let { it2 ->
                            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(
                                it1
                            ).child(it2)
                        }
                    }
                member?.allow_typing = Constans.DISABLE_TYPING
                member?.toMap()?.let { it1 -> query1?.updateChildren(it1) }
            }

            btnEnableTyping.setOnClickListener {
                btnEnableTyping.setVisibility(false)
                btnDisableTyping.setVisibility(true)
                val query1 =
                    group.gid?.let { it1 ->
                        member?.uid?.let { it2 ->
                            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(
                                it1
                            ).child(it2)
                        }
                    }
                member?.allow_typing = Constans.ENABLE_TYPING
                member?.toMap()?.let { it1 -> query1?.updateChildren(it1) }
            }

            btnApprove.setOnClickListener {
                btnEnableTyping.setVisibility(true)
                btnDisableTyping.setVisibility(false)
                val query1 =
                    group.gid?.let { it1 ->
                        member?.uid?.let { it2 ->
                            mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(
                                it1
                            ).child(it2)
                        }
                    }
                member?.status = Constans.STATUS_APPROVED
                member?.toMap()?.let { it1 -> query1?.updateChildren(it1) }
            }
        }
    }

    private fun View.setVisibility(visibility: Boolean) {
        if (visibility)
            this.visibility = View.VISIBLE
        else
            this.visibility = View.GONE

    }

    override fun getItemCount(): Int {
        return memberList?.count() ?: 0
    }

    fun setData(data: List<GroupUser>) {
        memberList = data
        notifyDataSetChanged()
    }
}