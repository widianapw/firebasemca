package com.example.firebasechat.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasechat.R
import com.example.firebasechat.data.group.Group
import kotlinx.android.synthetic.main.item_group.view.*

class GroupAdapter(val clickListener: (Group) -> Unit) :
    RecyclerView.Adapter<GroupAdapter.ViewHolder>() {
    private var groupList: List<Group>? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_group, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val group = groupList?.get(position)
        holder.itemView.apply {
            tvTitleGroup.text = group?.name
            tvStatus.text = group?.public_status
            cardGroup.setOnClickListener {
                group?.let { it1 -> clickListener(it1) }
            }
        }
    }

    override fun getItemCount(): Int {
        return groupList?.count() ?: 0
    }

    fun setData(data: List<Group>) {
        groupList = data
        notifyDataSetChanged()
    }
}