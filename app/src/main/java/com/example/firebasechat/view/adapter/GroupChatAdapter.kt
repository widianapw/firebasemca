package com.example.firebasechat.view.adapter

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.common.GlobalClass.Companion.context
import com.example.firebasechat.data.group.GroupChat
import com.example.firebasechat.view.activity.PhotoViewerActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_chat_left.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*


class GroupChatAdapter(val fileDownloadListener: (GroupChat) ->Unit) : RecyclerView.Adapter<GroupChatAdapter.ViewHolder>() {
    private val mUser = FirebaseAuth.getInstance().currentUser
    private var messageList: List<GroupChat>? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == Constans.MSG_ENGINE)
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_chat_left, parent, false)
            )
        else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_chat_right, parent, false)
            )
    }

    override fun getItemViewType(position: Int): Int {
        return if (messageList?.get(position)?.uid == mUser?.uid) {
            Constans.MSG_SELF
        } else {
            Constans.MSG_ENGINE
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chat = messageList?.get(position)
        if (position != 0) {
            if (itemCount > 1) {
                val currentDate = chat?.timestamp?.toLong()?.let { getChatDate(it) }
                val nextDate = messageList?.get(position - 1)?.timestamp?.toLong()?.let {
                    getChatDate(
                        it
                    )
                }
                if (currentDate != nextDate) {
                    holder.itemView.textDate.visibility = View.VISIBLE
                } else {
                    holder.itemView.textDate.visibility = View.GONE
                }
            }
        } else {
            holder.itemView.textDate.visibility = View.VISIBLE
        }

        holder.itemView.apply {
            textSender.text = chat?.email
            textDate.text = chat?.timestamp?.toLong()?.let { getChatDate(it) }

            when (chat?.message_type) {
                Constans.MESSAGE_TYPE_CHAT -> {
                    textChat.text = chat.message
                    imgChat.visibility = View.GONE
                    fileChat.visibility = View.GONE
                    textChat.visibility = View.VISIBLE
                }
                Constans.MESSAGE_TYPE_IMG -> {
                    imgChat.visibility = View.VISIBLE
                    textChat.visibility = View.GONE
                    fileChat.visibility = View.GONE
                    Glide.with(this).load(chat.message).placeholder(R.color.colorAlto).into(imgChat)
                }
                else -> {
                    imgChat.visibility = View.GONE
                    fileChat.visibility = View.VISIBLE
                    textChat.visibility = View.GONE
                    tvFileName.text = chat?.file_name
                }
            }

            textTime.text = chat?.timestamp?.toLong()?.let { getChatTime(it) }
            imgChat.setOnClickListener {
                context.startActivity<PhotoViewerActivity>(
                    Constans.IMG_PATH to chat?.message
                )
            }
            fileChat.setOnClickListener {

                chat?.let { it1 -> fileDownloadListener(it1) }

            }
        }

    }

    override fun getItemCount(): Int {
        return messageList?.count() ?: 0
    }

    private fun getChatTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("HH:mm:ss")
        return format.format(date)
    }


    private fun getChatDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("EEE, dd MMM yyyy")
        return format.format(date)
    }

    fun setData(data: List<GroupChat>) {
        messageList = data
        notifyDataSetChanged()
    }


}