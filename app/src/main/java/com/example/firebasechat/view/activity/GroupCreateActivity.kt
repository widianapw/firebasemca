package com.example.firebasechat.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_group_create.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton

class GroupCreateActivity : AppCompatActivity() {
    private val mUser = FirebaseAuth.getInstance().currentUser
    private val mDatabase =
        FirebaseDatabase.getInstance().reference

    var isInsert = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_create)
        toolbarGroupCreate.setNavigationOnClickListener {
            onBackPressed()
        }

        val items = resources.getStringArray(R.array.group_statis)
        val adapter = ArrayAdapter(this, R.layout.list_item, items)
        spinnerGroupStatus.setAdapter(adapter)
        validateInput()

        buttonCreate.setOnClickListener {
            checkGroupCode(etGroupCode?.text.toString())
        }
    }

    private fun checkGroupCode(code: String) {
        isInsert = true
        var flag = 0
        val query = mDatabase.child(Constans.GROUP_TABLE_FIREBASE)
        query.addValueEventListener(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (i in snapshot.children) {
                    if (i.child(Constans.GROUP_CODE).value.toString() == code && isInsert) {
                        flag += 1
                    }
                }
                if (flag == 0 && isInsert) {
                    insertGroup()
                } else if (isInsert && flag > 0) {
                    showGroupCodeTakenDialog()
                }
                flag = 0
                isInsert = false
            }
        })

    }

    fun showGroupCodeTakenDialog() {
        alert("Group code has been taken, please use another group code") {
            yesButton {
                it.dismiss()
            }
            isCancelable = false
        }.show()
        buttonCreate?.isEnabled = false

    }

    private fun insertGroup() {
        val groupId = mDatabase.child(Constans.GROUP_TABLE_FIREBASE).push().key
        val data = HashMap<String, Any?>()
        data[Constans.GROUP_ID_FIELD] = groupId
        data[Constans.GROUP_CODE] = etGroupCode.text.toString()
        data[Constans.NAME_FIELD] = etGroupName.text.toString()
        data[Constans.PUBLIC_STATUS_FIELD] = spinnerGroupStatus.text.toString()
        data[Constans.ALLOW_TYPING_FIELD] = Constans.ENABLE_TYPING
        data[Constans.ADMIN_FIELD] = mUser?.email
        Logger.addLogAdapter(AndroidLogAdapter())
        Logger.d(data)
        groupId?.let { it1 ->
            mDatabase.child(Constans.GROUP_TABLE_FIREBASE).child(it1).setValue(data)
                .addOnSuccessListener {
                    val data1 = HashMap<String, Any?>()
                    data1[Constans.UID_FIELD] = mUser?.uid
                    data1[Constans.EMAIL_FIELD] = mUser?.email
                    data1[Constans.ALLOW_TYPING_FIELD] = Constans.ENABLE_TYPING
                    data1[Constans.STATUS_FIELD] = Constans.STATUS_APPROVED
                    data1[Constans.IS_ADMIN_FIELD] = Constans.SUPER_ADMIN
                    mUser?.uid?.let { it2 ->
                        mDatabase.child(Constans.GROUP_USER_TABLE_FIREBASE).child(groupId).child(
                            it2
                        ).setValue(data1)
                            .addOnSuccessListener {
                                finish()
                                startActivity<AdminGroupActivity>(Constans.ADMIN_FIELD to 0)
                            }
                    }
                }
        }

    }

    private fun validateInput() {
        var isValidName = false
        var isValidStatus = false
        var isValidGroupCode = false
        etGroupCode.doOnTextChanged { text, _, _, _ ->
            isValidGroupCode = text?.trim()?.isNotEmpty()!!
            setEnabledButton(isValidName, isValidStatus, isValidGroupCode)
        }
        etGroupName.doOnTextChanged { text, _, _, _ ->
            isValidName = text?.trim()?.isNotEmpty()!!
            setEnabledButton(isValidName, isValidStatus, isValidGroupCode)
        }
        spinnerGroupStatus.doOnTextChanged { text, _, _, _ ->
            isValidStatus = text?.trim()?.isNotEmpty()!!
            setEnabledButton(isValidName, isValidStatus, isValidGroupCode)
        }
        buttonCreate.isEnabled = isValidName && isValidStatus && isValidGroupCode
    }

    private fun setEnabledButton(
        validName: Boolean,
        validStatus: Boolean,
        validGroupCode: Boolean
    ) {
        buttonCreate.isEnabled = validName && validStatus && validGroupCode
    }
}