package com.example.firebasechat.data.handler

import android.util.Log
import com.example.firebasechat.common.Utilities
import com.example.firebasechat.presenter.BaseContract
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import com.example.firebasechat.data.BaseResultData
import retrofit2.HttpException
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class ErrorHandler<T : BaseResultData>(presenter: BaseContract.Presenter) : Observer<T> {
    private val TAG = "APIError Handler"
    override fun onComplete() {
        Log.d("ProfileDisplayPresenter", "Data sudah disini complete ")
    }

    override fun onSubscribe(d: Disposable) {
        Log.d("ProfileDisplayPresenter", "Data sudah disini dis ")
    }

    private val weakReference: WeakReference<BaseContract.Presenter> = WeakReference(presenter)
    override fun onError(e: Throwable) {
        val presenter = weakReference.get()
        when (e) {
            is HttpException -> {
                val error = e.response()?.let { Utilities.parseError(it) }
                Log.i(TAG, e.response()?.code().toString())
                when (e.response()?.code()) {
                    403 -> presenter?.showError(
                        "Error",
                        "Forbidden"
                    )
                    422 -> presenter?.showError(
                        "Error",
                        "Unprocessed Entity"
                    )
                    else -> presenter?.showError(
                        "Error",
                        "Forbidden"
                    )
                }

            }
            is UnknownHostException -> {
                Log.i(TAG, "No connection")
                presenter?.showError(
                    "Error",
                    "No Internet Connection"
                )
            }
            is SocketTimeoutException -> {
                Log.i(TAG, "Timeout == ${e.cause}")
                presenter?.showError(
                    "Error",
                    "Connection Timed Out"

                )
            }
            is IOException -> {
                Log.i(TAG, "IO Exception == ${e.localizedMessage}")
                presenter?.showError(
                    "Error",
                    "IO Exception"
                )
            }
            else -> {
                Log.i(TAG, e.localizedMessage)
            }
        }
    }

}
