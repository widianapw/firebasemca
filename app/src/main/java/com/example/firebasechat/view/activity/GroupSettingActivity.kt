package com.example.firebasechat.view.activity

import android.content.ClipData
import android.content.ClipboardManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.group.Group
import com.example.firebasechat.view.ValueEventListenerAbstract
import com.google.firebase.database.DataSnapshot
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_group_create.*
import kotlinx.android.synthetic.main.activity_group_setting.*
import org.jetbrains.anko.toast

class GroupSettingActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_setting)
        val group = intent.getParcelableExtra<Group>(Constans.GROUP_TABLE_FIREBASE)
        initView(group)

        btnUpdateGroup?.setOnClickListener {
            group?.public_status = spinnerGroupStatusEdit?.text.toString()
            Logger.addLogAdapter(AndroidLogAdapter())
            Logger.d(group)
            group?.gid?.let { it1 ->
                mDatabase.child(Constans.GROUP_TABLE_FIREBASE).child(it1)
                    .updateChildren(group.toMap())
            }?.addOnSuccessListener {
                toast("Updated")
                onBackPressed()
            }
        }
    }

    private fun initView(group: Group?) {
        toolbarGroupSetting?.apply {
            setNavigationOnClickListener { onBackPressed() }
            title = group?.name
        }
        tvGroupCode?.text = group?.group_code

        ibCopy?.setOnClickListener {
            val clipBoard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("groupCode", tvGroupCode?.text.toString())
            clipBoard.setPrimaryClip(clipData)
            toast("Copied to Clipboard")
        }

        val items = resources.getStringArray(R.array.group_statis)
        val adapter = ArrayAdapter(this, R.layout.list_item, items)
        spinnerGroupStatusEdit?.apply {
            setAdapter(adapter)
        }

        val query = group?.gid?.let { mDatabase.child(Constans.GROUP_TABLE_FIREBASE).child(it) }
        query?.addListenerForSingleValueEvent(object : ValueEventListenerAbstract() {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data = snapshot.getValue(Group::class.java)
                spinnerGroupStatusEdit?.setText(data?.public_status, false)
            }
        })
    }
}