package com.example.firebasechat.data.group

import android.os.Parcelable
import com.example.firebasechat.common.Constans
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Group(
    var gid: String?=null,
    var group_code: String?=null,
    var name: String?=null,
    var admin: String?=null,
    var public_status: String?=null,
    var allow_typing: String?=null
) : Parcelable{
    fun toMap(): Map<String, Any?> {
        return mapOf<String, Any?>(
            Constans.GROUP_ID_FIELD to gid,
            Constans.GROUP_CODE to group_code,
            Constans.NAME_FIELD to name,
            Constans.ADMIN_FIELD to admin,
            Constans.PUBLIC_STATUS_FIELD to public_status,
            Constans.ALLOW_TYPING_FIELD to allow_typing
        )
    }
}