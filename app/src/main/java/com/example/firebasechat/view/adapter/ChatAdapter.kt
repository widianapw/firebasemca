package com.example.firebasechat.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasechat.R
import com.example.firebasechat.common.Constans
import com.example.firebasechat.data.chat.Chat
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_chat_left.view.*
import java.text.SimpleDateFormat
import java.util.*

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {
    var chatList: List<Chat>? = null
    private val user = FirebaseAuth.getInstance().currentUser

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var textChat: TextView = itemView.findViewById(R.id.textChat)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == Constans.MSG_ENGINE)
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_chat_left, parent, false)
            )
        else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_chat_right, parent, false)
            )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chat = chatList?.get(position)
        if (position != 0) {
            if (itemCount > 1) {
                val currentDate = chat?.timestamp?.toLong()?.let { getChatDate(it) }
                val nextDate = chatList?.get(position - 1)?.timestamp?.toLong()?.let {
                    getChatDate(
                        it
                    )
                }
                if (currentDate != nextDate) {
                    holder.itemView.textDate.visibility = View.VISIBLE
                } else {
                    holder.itemView.textDate.visibility = View.GONE
                }
            }
        } else {
            holder.itemView.textDate.visibility = View.VISIBLE
        }


        holder.itemView.apply {
            textSender.visibility = View.GONE
            textDate.text = chat?.timestamp?.toLong()?.let { getChatDate(it) }
            textChat.text = chat?.message
            textTime.text = chat?.timestamp?.toLong()?.let { getChatTime(it) }
        }
    }

    override fun getItemCount(): Int {
        return chatList?.count() ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (chatList?.get(position)?.chatRoom == "0") {
            if (chatList?.get(position)?.messageType == Constans.MSG_SELF)
                Constans.MSG_SELF
            else
                Constans.MSG_ENGINE
        } else {
            if (chatList?.get(position)?.fromUid == user?.uid) {
                Constans.MSG_SELF
            } else {
                Constans.MSG_ENGINE
            }
        }
    }

    fun setData(data: List<Chat>) {
        chatList = data
        notifyDataSetChanged()
    }

    private fun getChatTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("HH:mm:ss")
        return format.format(date)
    }


    private fun getChatDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("EEE, dd MMM yyyy")
        return format.format(date)
    }
}